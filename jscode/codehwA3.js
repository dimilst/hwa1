function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];
}
    Hamburger.SIZE_SMALL = [50,20];
    Hamburger.SIZE_LARGE = [100,40];
    Hamburger.STUFFING_CHEESE = [10,20];
    Hamburger.STUFFING_SALAD = [20,5];
    Hamburger.STUFFING_POTATO = [15,10];
    Hamburger.TOPPING_MAYO = [20,5];
    Hamburger.TOPPING_SPICE = [15,0];

Hamburger.prototype.addTopping = function (topping) {
    try{
        var toppingExists = false;
    for(i=0; i<this.topping.length;i++){
        if (this.topping[i] === topping){
            toppingExists = true;
            break;
        }
    }
    if(!toppingExists) {
        this.topping.push(topping)
    }
    else {
        console.error("В бургере уже есть эта добавка");
    }
    }
    catch{
        console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.removeTopping = function (topping){
    try{
        for(j=0; j<this.topping.length;j++){
        if (this.topping[j] === topping){
            this.topping.splice(j,1);
        }
    }
    }
    catch{
        console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.getToppings = function (){
    try{
    var top = this.topping;
    return top;
    }
    catch{
    console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.getSize = function (){
    try{
    var s = this.size;
    return s;
    }
    catch{
    console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.getStuffing = function (){
    try{
    var t = this.stuffing;
    return t;
    }
    catch{
    console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.calculatePrice = function (){
    try{
    var p = this.size[0] + this.stuffing[0];
    for(k=0; k<this.topping.length; k++){
        p = p + this.topping[k][0];
    }
    return p;
    }
    catch{
    console.error("Некорректное использование блока");
    }
}

Hamburger.prototype.calculateCalories = function (){
    try{
    var c = this.size[1] + this.stuffing[1];
    for(d=0; d<this.topping.length; d++){
        c = c + this.topping[d][1];
    }
    return c;
    }
    catch{
    console.error("Некорректное использование блока");
    }
}

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Calories: %f", hamburger.calculateCalories());
console.log("Price: %f", hamburger.calculatePrice());
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with sauce: %f", hamburger.calculatePrice());
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log("Have %d toppings", hamburger.getToppings().length);