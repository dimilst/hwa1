function Human(name, age){
    this.name = name;
    this.age = age;
}
Human.prototype.print = function (){
    document.write(this.name + " " + this.age + " лет");
}

const alex = new Human("alex",24);
const vlad = new Human("vlad",25);
const serg = new Human("serg",19);

alex.print();

const a = new Array(alex, vlad, serg);

a.sort(function(c,b){
    return c.age-b.age;
});

Human.prototype.toString = function humanToString() {
    let res = this.name + " is " + this.age;
    return res;
}
document.write("<br>" + serg.toString());