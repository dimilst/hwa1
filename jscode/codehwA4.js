const createCircle = () =>{
    let diam = prompt("Введите диаметр");
    const list = document.getElementById("task1");
    list.setAttribute(`style`, `display: flex; flex-direction: row; flex-wrap: wrap; width: ${diam*10}px`);
        for(i=0; i<100; i++){
            let c = '#' + Math.floor(Math.random()*16777215).toString(16);
            const item = document.createElement("div");
            item.setAttribute(`style`, `width: ${diam}px; height: ${diam}px; border-radius: 50%; background-color: ${c}`);
            item.setAttribute(`id`, `item${i+1}`);
            item.addEventListener('click', (e) => {
                const toDelete = e.target;
                list.removeChild(toDelete);
              });
            list.appendChild(item);
        }
}

