//Смена стилей на случай мелкого дисплея
let w =  screen.width, h =  screen.height;
if (w <=  "800" || h <= "800"){
     document.getElementById("stylesheet").href="style/cwA5m.css";
}

window.addEventListener("DOMContentLoaded", () => {
    let number1 = "", number2 = "", number3 = 0, number4 = 0 , number5 = 0, number6 = 0;
    let check = false, flag = 0;

    const display = document.getElementById("screen");
    const numbers = document.getElementsByClassName("button black");
    const operations = document.getElementsByClassName("button pink");
    const result = document.getElementsByClassName("button orange");
    const mmm = document.getElementsByClassName("button gray");
    // Функция подсчета результатов
    const res = function(){
        if(flag==1){
            number5 = number3/number4;
            display.value = number5;
        }
        if(flag==2){
            number5 = number3*number4;
            display.value = number5;
        }
        if(flag==3){
            number5 = number3-number4;
            display.value = number5;
        }
        if(flag==4){
            number5 = number3+number4;
            display.value = number5;
        }
        number3 = number5;
        number2 = "";
    }
        // Кнопка обнуления значений
        numbers[11].addEventListener(`click`,() => {
        number1 = "";
        number2 = "";
        number3 = 0;
        number4 = 0;
        number5 = 0;
        number6 = 0;
        check = false;
        flag = 0;
        display.value = "";
    })
    // Кнопки ввода значений
    for(i=0; i<numbers.length-1; i++)
    {
        numbers[i].addEventListener(`click`,(e) => {
            if (!check){
            number1 = number1 + e.target.value;
            display.value = number1;
            number3 = Number(number1);
            }
            if(check){
            number2 = number2 + e.target.value;
            display.value = number2;
            number4 = Number(number2);
            }
        })
    }
        // Кнопка равно
        result[0].addEventListener(`click`,() => {
            res();
            flag = 0;
            check = !check;
        })
        // Кнопки математических выражений
        operations[0].addEventListener(`click`,() => {
        if(flag !=0){
            res();
            flag = 1;
        }
        else{
            check = !check;
           flag = 1;
        }
        })

        operations[1].addEventListener(`click`,() => {
            if(flag !=0){
                res();
                flag = 2;
            }
            else{
                check = !check;
               flag = 2;
            }
        })

        operations[2].addEventListener(`click`,() => {
            if(flag !=0){
                res();
                flag = 3;
            }
            else{
                check = !check;
               flag = 3;
            }
        })

        operations[3].addEventListener(`click`,() => {
            if(flag !=0){
                res();
                flag = 4;
            }
            else{
                check = !check;
               flag = 4;
            }
        })
        // Кнопки памяти
        mmm[0].addEventListener(`click`,() => {
            if (!check){
                number1 = number6;
                display.value = number1;
                number3 = Number(number1);
                }
            if(check){
                number2 = number6;
                display.value = number2;
                number4 = Number(number2);
                }
            })

        mmm[1].addEventListener(`click`,() => {
            number6 = number6 - Number(display.value);
            })
            
        mmm[2].addEventListener(`click`,() => {
            number6 = number6 + Number(display.value);
            })
});