window.addEventListener("DOMContentLoaded", ()=>{
    gen();
    const s = document.getElementById("str");
    s.addEventListener("click",start);
    const x = document.getElementById("surr");
    x.addEventListener("click",end);
    const e = document.getElementById("easy");
    e.addEventListener("click",easy);
    const m = document.getElementById("medium");
    m.addEventListener("click",medium);
    const h = document.getElementById("hard");
    h.addEventListener("click",hard);
});
let p = 0, c = 0, l = 0, timer = 1000, interval, arr = new Array();
//Функция старта игры
function start(){
    clearInterval(interval);
    const e = document.getElementById("easy");
    e.removeEventListener("click",easy);
    const m = document.getElementById("medium");
    m.removeEventListener("click",medium);
    const h = document.getElementById("hard");
    h.removeEventListener("click",hard);
    const r = document.getElementById("player");
    r.setAttribute(`value`,`0`);
    const g = document.getElementById("cp");
    g.setAttribute(`value`,`0`);
    p = 0;
    c = 0;
    l = 0;
    const f = document.getElementById("tb");
    f.innerHTML = "";
    for(o=0;0<arr.length;o++){
        arr.splice(0,1);
    }
    gen();
    interval = setInterval(work, timer);
    const s = document.getElementById("str");
    s.removeEventListener("click",start);
}
//Функции выора сложности
function easy(){
    timer = 1500;
}
function medium(){
    timer = 1000;
}
function hard(){
    timer = 500;
}
//Функция генератора поял
const gen = function(){
    for(q=0;q<100;q++){
        arr.push(q);
    }
    const field = document.getElementById("tb");
    for(i=0;i<10;i++){
    const raw = document.createElement("tr");
    field.appendChild(raw);
    for(j=0;j<10;j++){
        const h = document.createElement("th");
        const d = document.createElement("div");
        if(i==0){
            let k = String(j);
            d.setAttribute(`style`,`width: 60px; height: 60px; background-color: black`);
            d.setAttribute(`id`,`${k}`);
            raw.appendChild(h);
            h.appendChild(d);
        }
        else{
            let k = String(i) + String(j);
            d.setAttribute(`style`,`width: 60px; height: 60px; background-color: black`);
            d.setAttribute(`id`,`${k}`);
            raw.appendChild(h);
            h.appendChild(d);
        }
    }
    }
}
//Функция отвечающая за процесс игры
function work(){
    if(p<50&&c<49){
        blue();
    }
    else{
        end();
    }
}
//Функция окончания игры
function end(){
    clearInterval(interval);
    const e = document.getElementById("easy");
    e.addEventListener("click",easy);
    const m = document.getElementById("medium");
    m.addEventListener("click",medium);
    const h = document.getElementById("hard");
    h.addEventListener("click",hard);
    const s = document.getElementById("str");
    s.addEventListener("click",start);
    if(p == 50){
        alert("You won!");
    }
    else{
        alert("You lose!");
    }
}
//Генератор случайного числа
function randomInteger(min,max){
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }
//Счет игрока
const bb = function(e){
    e.target.setAttribute(`style`,`width: 60px; height: 60px; background-color: green`);
    p = ++p;
    const r = document.getElementById("player");
    r.setAttribute(`value`,`${p}`);
}
//Функция проверки для того что-бы случайный элемент не повторялся
function check(){
    let trb = false;
    for(y=0;y<arr.length;y++){
        if(l==arr[y]){
            arr.splice(y,1);
            trb = true;
        }
    }
}
//Функционал игры
function blue(){
    let trb = false;
    l = randomInteger(0,99);
    for(y=0;y<arr.length;y++){
        if(l==arr[y]){
            arr.splice(y,1);
            trb = true;
        }
    }
    if(trb == true){
        const di = document.getElementById(`${l}`);
        di.setAttribute(`style`,`width: 60px; height: 60px; background-color: blue`);
        di.addEventListener("click",bb);
        setTimeout(()=>{
            if(di.style.backgroundColor == "blue"){
                di.setAttribute(`style`,`width: 60px; height: 60px; background-color: red`);
                c = ++c;
                const g = document.getElementById("cp");
                g.setAttribute(`value`,`${c}`);
            }
            di.removeEventListener("click",bb);},timer);
    }
    else{
        work();
    }
}