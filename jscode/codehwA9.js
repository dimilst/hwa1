window.onload = e => {
    const xhr = new XMLHttpRequest();

    xhr.open(`GET`, `https://swapi.co/api/people/`); 

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            const data = JSON.parse(xhr.responseText);
            let h;
            data.results.forEach(function(a) {
                const home = new XMLHttpRequest();
                home.open(`GET`, `${a["homeworld"]}`);
                home.onreadystatechange = function(){
                if(home.readyState == 4 && home.status == 200){
                    const hd = JSON.parse(home.responseText);
                    h = hd["name"];
                    const page = document.getElementById(`start`);
                    const add = document.createElement(`div`);
                    add.setAttribute(`align`,`center`)
                    add.innerHTML = a["name"] + '<br>' + a["gender"] + '<br>' + h + '<br>';
                    page.appendChild(add);
                    if(a["starships"].length>=1){
                        const star = document.createElement(`input`);
                        star.setAttribute(`type`,`button`);
                        star.setAttribute(`value`,`Список кораблей`);
                        star.addEventListener(`click`,e=>{
                                const sl = document.createElement(`h3`);
                                sl.innerHTML = "Пилотируемые корабли";
                                a["starships"].forEach(function(b){
                                    const shipR = new XMLHttpRequest;
                                    shipR.open(`GET`,`${b}`);
                                    shipR.onreadystatechange = function(){
                                        if(shipR.readyState == 4 && shipR.status == 200){
                                            const ship = JSON.parse(shipR.responseText);
                                            const s = document.createElement(`div`);
                                            s.innerHTML = "<br>" + ship["name"];
                                            sl.appendChild(s);
                                        }
                                    }
                                    shipR.send();
                                });
                                const toDelete = e.target;
                                add.removeChild(toDelete);
                                add.appendChild(sl);
                        });
                        add.appendChild(star);
                    }
                }
            }
            home.send();
            });
            
    }
}
    xhr.send();
}
