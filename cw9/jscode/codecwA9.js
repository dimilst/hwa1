window.onload = e => {
    const xhr = new XMLHttpRequest();

    xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"); 

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            const data = JSON.parse(xhr.responseText);
            // Сортируем полученый массив
            function sortByRate(arr) {
                arr.sort((a, b) => a.rate > b.rate ? 1 : -1);
              }
            sortByRate(data);
            //заполняем нашу таблицу для каждого элемента который подходит под наше условие
            data.forEach((a,b) => {
            if(a.rate>=25){
            const start = document.getElementById("output");
            const raw = document.createElement("tr");
            start.appendChild(raw);
            //Заполняем элементы таблицы получеными данными
            for(c in data[b]){
            const line = document.createElement("td");
            line.innerHTML = data[b][c];
            raw.appendChild(line);
            }
            }
            });
        }
        if(xhr.status >= 400){
            document.write(`Error number ${xhr.status}`);
        }
    }

    xhr.send();
}
